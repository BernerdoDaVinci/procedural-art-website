// This file contains code and functions that are common to many/all of the generators
const canvas = document.getElementById('c');
const ctx = canvas.getContext('2d');

const pauseButton = document.getElementById('start');
const resetButton = document.getElementById('reset');
const downloadButton = document.getElementById('download');
const backgroundColorInput = document.getElementById('backgroundColor');
const foregroundColorInput = document.getElementById('foregroundColor');
const zoomInput = document.getElementById('zoom');
const alphaInput = document.getElementById('alpha');
const widthInput = document.getElementById('canvasWidth');
const heightInput = document.getElementById('canvasHeight');

pauseButton.onclick = startButtonPress;
resetButton.onclick = reset;
downloadButton.onclick = download;
backgroundColorInput.onchange = changeBackground;
zoomInput.onchange = updateZoom;
widthInput.onchange = updateCanvasSize;
heightInput.onchange = updateCanvasSize;

let loop;
let going = false;
let artMade = false;

updateCanvasSize();

function changeBackground() {
  if (!artMade) {
    reset(false);
  }
}

function startButtonPress() {
  going = !going;

  if (going) {
    pauseButton.innerHTML = "Stop";
    loop = setInterval(iterate, 0);
  } else {
    pauseButton.innerHTML = "Start";
    clearInterval(loop);
  }
}

function updateZoom() {
  canvas.style.width = zoomInput.value * 0.01 * widthInput.value;
}

function updateCanvasSize() {
  canvas.width = document.getElementById("canvasWidth").value;
  canvas.height = document.getElementById("canvasHeight").value;

  updateZoom();
}

function reset(doConfirm) {
  let doReset = false;

  if (doConfirm && artMade) {
    if (confirm("Are you sure you want to clear the canvas?")) {
      doReset = true;
      artMade = false;
    }
  } else {
    doReset = true;
    artMade = false;
  }

  if (doReset) {
    ctx.fillStyle = backgroundColorInput.value;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    // Specifically for chaos games because I don't know javascript
    try {
      position = chaosPoints[Math.floor(Math.random()*chaosPoints.length)];
    } catch (e) {}
    
    // Specifically for flow field beacuse I don't know how to javascript
    try {
      flowPoints = [];
      noise = openSimplexNoise(Date.now() * (Math.random() + 1));
    } catch (e) {}
  }
}

function setForegroundColor() {
  let alpha = Number(alphaInput.value).toString(16);

  if (alpha.length == 1) {
    alpha = "0" + alpha;
  }

  ctx.fillStyle = foregroundColorInput.value + alpha;
  ctx.strokeStyle = foregroundColorInput.value + alpha;
}

function download() {
  var image = canvas.toDataURL("image/png")
    .replace("image/png", "image/octet-stream");
  downloadButton.setAttribute("href", image);
}

reset(false);
