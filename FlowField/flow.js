const resetPointsButton = document.getElementById('resetPoints');
const numPointsInput = document.getElementById('numOfPoints');
const fieldZoomInput = document.getElementById('fieldZoom');
const zOffChangeInput = document.getElementById('fieldChangeSpeed');
const fieldResolutionInput = document.getElementById('resolution');
const maxVelocityInput = document.getElementById('maxVelocity');
const fieldPowerInput = document.getElementById('fieldPower');
const lineThicknessInput = document.getElementById('lineThickness');

resetPointsButton.onclick = resetPoints;

let fieldResolution;

let zOff = 0;
let field = [];

let flowPoints = [];

let noise = openSimplexNoise(Date.now() * (Math.random() + 1));

function iterate() {
  artMade = true;

  fieldResolution = Number(fieldResolutionInput.value);
  let numOfPoints = numPointsInput.value;

  updateField();

  while (flowPoints.length < numOfPoints) {
    flowPoints.push(new Point(Math.random() * canvas.width, Math.random() * canvas.height));
  }

  while (flowPoints.length > numOfPoints) {
    flowPoints.pop();
  }

  setForegroundColor();
  ctx.lineWidth = lineThicknessInput.value;

  for (let i = 0; i < flowPoints.length; i++) {
    let doDelete = flowPoints[i].update()

    flowPoints[i].show();

    if (doDelete) {
      flowPoints.splice(i, 1);
      i--;
    }
  }
  // renderField();
  zOff += Number(zOffChangeInput.value);
}

function updateField() {
  let fieldSizeMultiplier = Number(fieldZoomInput.value);

  field = [];
  let column = [];
  for (let x = 0; x < canvas.width; x += fieldResolution) {
    column = [];
    for (let y = 0; y < canvas.height; y += fieldResolution) {
      column.push(noise.noise3D(x * fieldSizeMultiplier, y * fieldSizeMultiplier, zOff) * 10 * Math.PI);
    }
    field.push(column);
  }
}

function renderField() {
  reset(false);

  for (let x = 0; x < field.length; x++) {
    for (let y = 0; y < field[x].length; y++) {
      let xStart = x * fieldResolution;
      let yStart = y * fieldResolution;
      let xEnd = Math.cos(field[x][y]) * fieldResolution + xStart;
      let yEnd = Math.sin(field[x][y]) * fieldResolution + yStart;
      ctx.beginPath();
      ctx.moveTo(xStart, yStart);
      ctx.lineTo(xEnd, yEnd);
      ctx.stroke();
    }
  }
}

function resetPoints() {
  flowPoints = [];
}
