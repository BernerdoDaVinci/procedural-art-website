class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.lastX = x;
    this.lastY = y;
    this.velX = 0;
    this.velY = 0;
  }

  update() {
    this.lastX = this.x;
    this.lastY = this.y;

    let xArr = Math.floor(this.x / fieldResolution);
    let yArr = Math.floor(this.y / fieldResolution);

    let fieldPower = Number(fieldPowerInput.value);

    try {
      this.velX += Math.cos(field[xArr][yArr]) * fieldPower;
      this.velY += Math.sin(field[xArr][yArr]) * fieldPower;
    } catch (e) {
      let i = 0;
      while (this != flowPoints[i]) {
        i++;
      }

      return true;
    }

    let v = Math.sqrt(this.velX ** 2 + this.velY ** 2);

    let maxVelocity = Number(maxVelocityInput.value);
    if (v > maxVelocity) {
      this.velX *= maxVelocity / v;
      this.velY *= maxVelocity / v;
    }

    this.x += this.velX;
    this.y += this.velY;

    return false;
  }

  show() {
    ctx.beginPath();
    ctx.moveTo(this.lastX, this.lastY);
    ctx.lineTo(this.x, this.y);
    ctx.stroke();
    ctx.closePath();
  }
}
