const pointsButton = document.getElementById('setPoints');
const iterationsInput = document.getElementById('iterations');
const movePercentInput = document.getElementById('movePercent');

pauseButton.onclick = customStartButtonPress;
canvas.onclick = clickCanvas;
pointsButton.onclick = setPoints;

let position;
let canSetPoints = false;
let chaosPoints = [];

function iterate() {
  artMade = true;

  setForegroundColor();

  for (let i = 0; i < iterationsInput.value; i++) {
    let nextPoint = chaosPoints[Math.floor(Math.random()*chaosPoints.length)];
    let moveMultiplier = movePercentInput.value * 0.01;

    nextX = (nextPoint[0] - position[0]) * moveMultiplier + position[0];
    nextY = (nextPoint[1] - position[1]) * moveMultiplier + position[1];

    position = [nextX, nextY];

    ctx.fillRect(position[0], position[1], 1, 1);
  }
}

function customStartButtonPress() {
  startButtonPress();

  if (canSetPoints) {
    setPoints();
  }
}

function setPoints() {
  canSetPoints = !canSetPoints;
  if (canSetPoints) {
    reset(true);
    pointsButton.innerHTML = "Finish Setting Points";
    chaosPoints = [];
    if (going) {
      startButtonPress();
    }
  } else {
    reset(false);
    pointsButton.innerHTML = "Set Points";
    position = chaosPoints[Math.floor(Math.random()*chaosPoints.length)];
  }
}

function showPoints() {
  ctx.fillStyle = "#f00";
  for (let i = 0; i < chaosPoints.length; i++) {
    let radius = 5 * Math.pow(zoomInput.value * 0.01, -1);
    ctx.beginPath();
    ctx.arc(chaosPoints[i][0], chaosPoints[i][1], radius, 0, 2 * Math.PI);
    ctx.fill();
  }
}

function clickCanvas(e) {
  if (canSetPoints) {
    let zoomMultiply = Math.pow(zoomInput.value * 0.01, -1);
    chaosPoints.push([e.offsetX * zoomMultiply, e.offsetY * zoomMultiply]);
    showPoints();
  }
}
